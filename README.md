# centos-rust-mini

A Docker image based on CentOS including a minimal Rust toolchain.

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`
